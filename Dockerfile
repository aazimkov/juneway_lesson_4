#building nginx
FROM debian:9 as build

ENV ngver=1.19.3

RUN apt update \
&& apt install -y wget gcc make libpcre3-dev zlib1g-dev

RUN wget https://nginx.org/download/nginx-$ngver.tar.gz \
&& tar zxvf nginx-$ngver.tar.gz

RUN cd nginx-$ngver \
&& ./configure \
--with-http_gzip_static_module \
&& make && make install

#creating leightweight debian image with nginx
FROM debian:9

WORKDIR /usr/local/nginx/sbin

COPY --from=build /usr/local/nginx/sbin/nginx .

RUN mkdir ../logs ../conf \
&& touch ../logs/error.log ../logs/access.log \
&& chmod +x nginx \
&& ln -s /usr/local/nginx/sbin/nginx /usr/local/sbin/nginx

CMD ["nginx", "-g", "daemon off;"]
